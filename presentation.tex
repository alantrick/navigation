\documentclass[aspectratio=2217]{beamer}

\usepackage[british]{babel}
\usepackage{graphicx,hyperref,acc,url,textcomp,gensymb}
\usepackage[space]{grffile}
\usepackage[utf8]{inputenc}

\newcommand<>{\fullgraphicframe}[1]{
{\setbeamercolor{background canvas}{bg=black}
{\setbeamertemplate{background}{}
  \begin{frame}[plain]
  \makebox[\linewidth]{
  \includegraphics[keepaspectratio,width=\paperwidth,height=\paperheight]{#1}
  }
  \end{frame}
  }}
}

\newcommand<>{\graphicframe}[2]{
  \begin{frame}
  \frametitle{#1}
  \begin{center}
  \includegraphics[keepaspectratio,width=.95\textwidth,height=.95\textheight]{#2}
  \end{center}
  \end{frame}
}

\title[Navigation \& Route-finding]{
  Navigation and Route-finding}


\author[Alan Trick]{
  \href{https://alantrick.ca}{Alan Trick}}

\institute[Alpine Club of Canada]{
  \href{http://accvancouver.ca/}{Alpine Club of Canada -- Vancouver Section}}

\date[2019 April 26]{Prepared 2019 April 26}

\begin{document}

\begin{frame}[plain]
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Outline}
  \tableofcontents
\end{frame}

\section{Introduction}
\begin{frame}
  \frametitle{Introduction}

  \begin{block}{Goal}
    To teach basic navigation skills so that trip organizers feel confident that they can plan safe and appropriate trips.
  \end{block}

  \begin{block}{Prerequisites}
    This workshop assumes a basic knowledge of mountaineering.
  \end{block}
\end{frame}

\begin{frame}
  \frametitle{Stages of navigation}
  \begin{itemize}
    \item Route Planning: what you do at home or at camp, typically involves maps, the internet, and maybe books
    \item Following a planned route
    \item Recovering after losing a planed route
  \item Making an ad-hoc route
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Scale}
\begin{tabular}{ r l p{190pt} }
    Name & Unit & Description \\ \hline
    Macro-terrain & 100m & The scale that planning happens at,
    maps aren't really much good below this scale. \\
    Micro-terrain & 10m & The scale that most routefinding happens. Significant
    obstacles like cliff bands or avalanche-prone slops are at this level. \\
    Nano-terrain & 1m & The scale that you actually move at. Things like
    ``how do I get over this log?''. \\
\end{tabular}

\vspace{1em}
Trying to keep track of 2 or 3 different scales at the same time can be difficult.
\end{frame}

\section{Route Planning}


\begin{frame}
  \frametitle{How to get information}
  \begin{itemize}
    \item The Internet: info can be sporadic and arbirtary, but it's the most convenient and often high quality info, skews towards popular and overcrowded areas
    \item Guidebooks: less common, but still pretty relevant, especially for less common routes
    \item Maps
    \item Reconnaissance trips
  \end{itemize}
\end{frame}

\subsection{Maps}

\begin{frame}
  \frametitle{Maps}
  \begin{itemize}
    \item Different maps prioritize different information 
    \item Maps can be inaccurate or misleading
    \item Humans are prone to biases when we see things and may misidentify features on a map
  \end{itemize}
\end{frame}

\graphicframe{Known World (Eratosthenes, 194 BCE)}{Eratosthenes, 194 BCE.jpg}
\graphicframe{Known World (Mercator, 1569 CE)}{Mercator, 1569 CE.png}

\begin{frame}
  \frametitle{Geographic coordinate systems}
  \begin{columns}
    \begin{column}{.45\textwidth}
    Latitude \& Longitude
      \begin{itemize}
        \item Works globally
        \item Hard to use on paper maps (size changes with latitude)
        \item Different datums (WGS 84, NAD 27, GCJ-02)
        \item E.g. 49\degree{} 3\ensuremath{'} 0\ensuremath{''} N, 122\degree{} 19\ensuremath{'} 0\ensuremath{''} W or 49.05,~-122.316667
      \end{itemize}
    \end{column}
    \begin{column}{.55\textwidth}
      UTM ``Projection''
      \begin{itemize}
        \item Based on meters East \& North of some point, has 60 zones
        \item Typically given in an abbreviated form that specifies an area in a 100 km\textsuperscript{2} area
        \item Useful on paper maps
        \item E.g. 3496 (1 km\textsuperscript{2} grid), 345692 (100 m\textsuperscript{2} grid), or 34536921 (10 m\textsuperscript{2} grid)
      \end{itemize}
    \end{column}
  \end{columns}
  \begin{center}
  There are others \tiny{(but we're ignoring them)}    
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Maps for outdoor recreation}
  \begin{itemize}
    \item Important features
    \begin{enumerate}
      \item trails/paths
      \item peaks
      \item waterways
      \item land-cover: forest/glaciers
      \item cliffs/elevation
      \item campsites
    \end{enumerate}
    \item Ways of handling elevation \& slope
    \begin{itemize}
      \item topographic lines
      \item hill-shading
      \item relief (by height or by slope)
    \end{itemize}
  \end{itemize}
\end{frame}

\graphicframe{Seymour (Canvec)}{seymour canvec.png}
\graphicframe{Seymour (NRCan)}{seymour nrcan.png}
\graphicframe{Seymour (CanMatrix)}{seymour canmatrix.png}
\graphicframe{Seymour (Google Map)}{seymour google map.png}
\graphicframe{Seymour (Google Terrain)}{seymour google terrain.jpg}
\graphicframe{Seymour (Google Satellite)}{seymour google satellite.jpg}
\graphicframe{Seymour (openstreetmap.org, OSM)}{seymour osm carto.png}
\graphicframe{Seymour (ThunderForest, OSM)}{seymour osm tf.png}

\subsection{Important Features}

\begin{frame}
  \frametitle{Trails/paths}

  \begin{itemize}
    \item What counts as a path can vary significantly (paved cycleway, non-motorized gravel road, 30 year old mining trail, route over an icy lake, scrambling route).
    \item Usually details about the amount of climbing, bushwacking, or routefinding involved are absent.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Peaks, ridges, and waterways}

  \begin{itemize}
    \item Waterways a good way to figure out where the valleys are located.
    \item Ridges and valleys are usually not represented on maps, but they can be used for getting a sense of the elevation if they're there.
    \item Peaks are really useful for orientation, they're fairly distinct and usually the easiest things to spot.
    \item Waterways are also useful if you're thirsty.
    \item Dry waterway, especially when they're small, often look like paths. This is a very common way to get lost.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Land-cover: forest, glaciers, etc}
  \begin{columns}
    \begin{column}{.75\textwidth}
      \begin{itemize}
        \item Knowing where the forests are is useful both for bushwhacking \& avalanche mitigation.
        \item Avoid entering \& exiting a forest too much unless the undergrowth is covered (like it is by the snow in the winter)
        \item Forests vary significantly with the biome.
        \item Glaciers are useful for obvious reasons: hazards, water source. They may be significantly receded if based on old data, though.
        \item Wetlands (if mapped) are generally something you want to avoid.
      \end{itemize}
    \end{column}
    \begin{column}{.25\textwidth}
        \includegraphics[keepaspectratio,width=\textwidth,height=\textheight]{rohr avi paths.png}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Cliffs and elevation}
  \begin{columns}
    \begin{column}{.5\textwidth}
      \begin{itemize}
        \item Accuracy of elevation is somewhat important.
        \item Cliffs are rarely marked directly on a map, but if they are they are often useful as they are typically more precise.
      \end{itemize}
    \end{column}
    \begin{column}{.25\textwidth}
        \includegraphics[keepaspectratio,width=\textwidth,height=\textheight]{smith rocks opencycle.png}
    \end{column}
    \begin{column}{.25\textwidth}
        \includegraphics[keepaspectratio,width=\textwidth,height=\textheight]{smith rocks caltopo.png}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Campsites}
  \begin{columns}
    \begin{column}{.75\textwidth}
      \begin{itemize}
        \item What counts as a campsite can vary significantly (fully furnished with a hot tub vs small bivy site for 2 people)
        \item Marking of back-country camping areas on maps is often quite limited.
      \end{itemize}
    \end{column}
    \begin{column}{.25\textwidth}
        \includegraphics[keepaspectratio,width=\textwidth,height=\textheight]{camping.png}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Other landmarks}

  \begin{itemize}
    \item Power lines (and their accompanying towers)
    \item Pipeline cutlines
    \item Communication towers (often covered in `soda bottle'-like shelters in the mountains)
    \item Cabins/shelters
  \end{itemize}
\end{frame}

\subsection{The Plan Itself}

\begin{frame}
  \frametitle{The plan itself}
  An actual plan could or should contain:
  \begin{itemize}
    \item Starting \& ending locations; and how to get there
    \item Required gear \& supplies
    \item Outline of way-points \& ETAs
    \item Required technical skills
    \item Difficulty
    \item Minimum \& maximum number of people
    \item Bailing options
    \item Plan B
  \end{itemize}
\end{frame}


\subsection{Homework}

\begin{frame}
  \frametitle{Homework}
  \begin{itemize}
    \item Research the various trailheads for McKee Peak
    \item Research details about a particular (predefined) objective, come up with a plan
    %% Easy: Frosty Mountain, Mount Thurston, Mount Strachan, Widgeon Lake, Whirlwhind Peak, Panorama Ridge (Garibaldi Park), Eagle Mountain (Indian Arm Park)
    %% Harder: Robie Reid, Edge Peak, Guanaco Peak, Macdonald Peak, Mount Seaton, Judge Howay,
    %% Really hard: Mount Urquhart, Mount Vayu, Petlushkwohap Mountain, Longslog Mountain
  \end{itemize}
\end{frame}

\section{Following a Route}
\subsection{Navigation}
\begin{frame}
  \frametitle{Navigating by the seat of your pants}
  Going by the seat of your pants is a legitimate technique. It probably what you'll do most of the time. When you do this, bear a few things in mind:
  \begin{itemize}
    \item It only works when you're familiar with an area.
    \item It's important to be able to recognize when you might be off route---watch for things that don't match your expectations.
    \item Don't do this just because you're lazy or you don't want to look stupid.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Handrails, Landmarks, \& Backstops}
  \begin{columns}
    \begin{column}{.75\textwidth}
       If you look at a map ahead of time, you can often identify features that you can use as navigation queues.
      \begin{itemize}
        \item Handrails: contour along creek (A \& D) and follow ridgeline (F) from top of gulley to peak.
        \item Landmarks: cross two creeks (B) before turning right, go along side of large bolder field (E).
        \item Backstops: if you come to a junction of 3 streams (C), you've gone too far.
      \end{itemize}
    Note that reversing this should be relatively straightforward.
    \end{column}
    \begin{column}{.25\textwidth}
        \includegraphics[keepaspectratio,width=\textwidth,height=\textheight]{routefinding.png}
    \end{column}
  \end{columns}
\end{frame}

\subsection{Finding Yourself}

\begin{frame}
  \frametitle{Orientation}

  \begin{itemize}
    \item Whether it's from a compass, some electronics, analyzing the moss on trees, or just familiarity with an area; orienting yourself is important.
    \item Dis-orientation generally happens over time---don't let it.
    \item ``Sense of direction'' is largely a trick your mind plays on itself. Sometimes it works.
    \item Practice regularly
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Orientation without tools}
  \begin{itemize}
    \item Use prior knowledge.
    \item Use the sun: it's due south about 1--1:30 PM in Vancouver (12:30--1 PM in Golden). Note that it might be more pleasant to use the direction of a shadow than the sun itself.
    \item In the night, there's the North Star (requires good weather and good eyes).
    \item In the alpine you can sometimes use tall, distinct peaks like Mount Baker (it's tricky though).
    \item Sometimes the direction of the ocean works.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Orientation with tools}
  \begin{itemize}
    \item Common tools: map, GPS, compass, barometric altimeter
    \item General technique
    \begin{itemize}
        \item With GPS: Hope you don't run out of batteries.
        \item Without GPS
        \begin{enumerate}
            \item Find \& determine direction (and possibly distance) of landmarks.
            \item Plot points on map and draw lines based on direction.
            \item Location is at intersection.
            \item Ideally you have at least 3 landmarks or 2 landmarks and some other information for error checking. More is better.
            \item Orient the map, so that north faces north or the landmarks are in the right direction.
        \end{enumerate}
    \end{itemize}
  \end{itemize}
\end{frame}

\subsection{Tools}
\begin{frame}
  \frametitle{Compasses}
  Compasses use magnetic north and maps use true north. Magnetic north changes from time to time. The differences between these is called magnetic declination, and is currently about 16 \degree{}W in Southwest BC. Nice compasses will allow you to adjust themselves to account for declination. If you don't have this, you'll have for manually account for the difference.
\end{frame}
\begin{frame}
  \frametitle{Compasses: Orienting a Map}
  \begin{enumerate}
      \item Line the ``orienting lines'' on the back of the ``compass housing'' to match the north-south lines on the map.
      \item Line the red north needle with the red arrow on the back of the housing.
  \end{enumerate}
\end{frame}
\begin{frame}
  \frametitle{Compasses: Finding a bearing}
  \begin{enumerate}
      \item Point front of compass toward target.
      \item Rotate compass housing so that the red north needle.
      \item Lines up with the red arrow on the back of the housing.
      \item The bearing at the front of the compass is your bearing.
  \end{enumerate}
  If you have a mirror, you can use this to watch your needle alignment while holding the compass level with your eyes.
\end{frame}

\begin{frame}
  \frametitle{Sources of error in orientation}
  \begin{itemize}
    \item GPS
    \begin{itemize}
        \item Shiny (e.g. glass) buildings
        \item Steep slopes
        \item Tree cover
        \item Cloud cover
    \end{itemize}
    \item Compass
    \begin{itemize}
        \item Needle sticking
        \item Incorrect declination
        \item North/south mix-up
        \item Mismatched north/south lines
    \end{itemize}
    \item Other
    \begin{itemize}
        \item Visual misidentification
        \item Error in guess-work
        \item Barometric altimeter: changes in weather
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Sample Map Plots}
  \begin{columns}
    \begin{column}{.5\textwidth}
      \begin{itemize}
        \item All points: elevation about 2150~m
        \item Point A \& B: 1 peak directly ESE
        \item Point A
        \begin{itemize}
            \item 2 peaks about 1~km WNW
            \item Small lake W (possibly hidden)
        \end{itemize}
        \item Point B: 1 peak about 500~m WNW
        \item Point C
        \begin{itemize}
            \item 1 peak directly W
            \item Small lake about 1~km south \& 500~m down
            \item 1 peak about 1~km E
        \end{itemize}
      \end{itemize}
    \end{column}
    \begin{column}{.5\textwidth}
        \includegraphics[keepaspectratio,width=\textwidth,height=\textheight]{orientation.png}
    \end{column}
  \end{columns}
\end{frame}


\section{Getting Lost}
\begin{frame}
  \frametitle{How to not get lost}
  \begin{itemize}
    \item Don't push your limits too hard without appropriate preparation: ``The tall pine's more often shaken by the wind\ldots{}'' (Horace).
    \item While on route, regularly take a moment to think about where you are.
    \item Maintain good communication with your party, others might notice things that you don't.
    \item Pay attention when important details change unexpectedly (like the trail becomes overgrown).
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{What to do when you're lost}
  \begin{itemize}
    \item Take short break to take stock of the situation. Think about how you got yourself here, both literally and figuratively.
    \item Decide whether to find your way back or call for help \& seek shelter.
    \item Prefer objective facts over intuition when trying to orient yourself.
    \item Try to get on top of a ridge or follow natural features \& landmarks. Try to match these features to your map.
  \end{itemize}
\end{frame}

\section{Ad-hoc Routes}
\begin{frame}
  \frametitle{Bushwacking}
  \begin{itemize}
    \item Generally easier:
    \begin{itemize}
        \item In old growth forest
        \item On gentle ridgelines
        \item With good snow cover
    \end{itemize}
    \item Generally harder:
    \begin{itemize}
        \item In snow-less avalanche paths (thick bush)
        \item Going uphill on steep terrain
        \item When you're carrying lots of gear
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{In the alpine}
  \begin{itemize}
    \item Fortunately, you typically don't have thick bush.
    \item Unfortunately, you typically have lots of cliffs or crevasses.
    \item Rock typically doesn't leave much evidence of human traffic and tracks on snow can disappear with a little wind/snow.
    \item Too much micro-terrain makes most maps only marginally useful.
    \item On difficult rock routes, try to find a ``topo'' (not to be confused with a topo map).
    \item Always plan for bail options.
    \item Always climb below your ability level.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Dealing with low visibility}
  \begin{itemize}
    \item If possible, find a feature that your can contour along (like a lake, stream, cliff)
    \item Take bearings as regularly as possible to get a sense of your general direction.
    \item The above is especially true when following a trail in a forest, as they often meander and individual readings may vary a lot.
    \item Have your partner (or the other end of the rope team) as far away as possible, and then take their position.
    \item It's more important to stay on safe \& easy terrain than to maintain a straight line.
    \item In snow, use wands. It's best to place these before visibility is a problem, and make sure they won't get blown over by the wind.
  \end{itemize}
\end{frame}

\section{Appendix}
\begin{frame}
  \frametitle{Appendix 1: Online Resources for BC}
  \begin{columns}
    \begin{column}{.5\textwidth}
      Information
      \begin{itemize}
        \item \href{http://forums.clubtread.com}{forums.clubtread.com}
        \item \href{https://cascadeclimbers.com/}{cascadeclimbers.com}
        \item \href{https://mountain-forecast.com}{mountain-forecast.com}
        \item Your favourite search engine
        \item \href{https://bivouac.com}{bivouac.com}
      \end{itemize}
    \end{column}
    \begin{column}{.5\textwidth}
      Maps
      \begin{itemize}
        \item \href{https://caltopo.com}{caltopo.com}
        \item \href{https://opentopomap.org}{opentopomap.org}
        \item \href{https://opencyclemap.org}{opencyclemap.org}
        \item \href{https://maps.google.com}{maps.google.com}
        \item \href{https://www.bing.com/maps}{www.bing.com/maps}
        \item \href{https://www.trailforks.com/}{www.trailforks.com}
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Appendix 2: Mobile Software}
  This list is pretty short. There are probably many other
  fine options, these are just the one's I've heard of that
  are half-decent.
  \begin{itemize}
    \item OsmAnd (with contour lines plugin)
    \item Gaia GPS
    \item Komoot
    \item OruxMaps
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Appendix 3: Recommended Reading}
  You can find these titles at many outdoor stores.
  \begin{itemize}
    \item Bob Burns, Mike Burns, \textit{Wilderness Navigation}, 2015, 3rd ed, ISBN 9781594859458. A fairly technical book about how to use maps \& compasses and other tools.
    \item Tristan Gooley, \textit{The Natural Navigator}, 2012, ISBN 9781615190461. An in-depth description of how to practically navigate using the terrain around you.
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Appendix 4: Miscellaneous}
\begin{itemize}
\item Radios: can be nice, but recreational ones don't work in a lot of cases
\item Splitting up: don't (unless you need to)
\item Mobile devices with touch screens suck in storms
\end{itemize}
\end{frame}

\end{document}

%% Schedule Wednesday, Saturday, Sunday
%% day 1: theory 2-3 hours
%%        practice identifying utm coordinates on a map
%% day 2: theory 1-2 hours
%%        practice 3 hours at Downes bowl (treasure hunt)
%%        practice 4 hours at Mckee Peak
%% day 3: theory 1 hour
%%        practice 6 hours
%% Tasks:
%% * checking bearing of various POI
%% * route finding at mckee peak, hike to random place and then require people to figure out where they are
%%
%% Game ideas:
%% * paper bags, follow a route & back
%% * paper bags but with compass
%%
%% Advanced practice areas
%% * Markhor Peak
%% * Mount Nutt
%% * Stonerabbit ?
%% * Pretty Peak
%% * Crickmer via Blue Mountain area